<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstallTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
        });

        Schema::create('adverts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('login')->unique();
            $table->string('password');
            $table->timestamps();
        });

        Schema::create('goods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->integer('advert_id');
            $table->timestamps();

            //$table->foreign('advert_id')->references('id')->on('adverts');
        });


        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id');
            $table->integer('good_id');
            $table->string('client_phone');
            $table->string('client_name');
            $table->timestamps();

            //$table->foreign('status_id')->references('id')->on('status');
            //$table->foreign('good_id')->references('id')->on('goods');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('status');
        Schema::drop('adverts');
        Schema::drop('goods');
        Schema::drop('orders');
    }
}
