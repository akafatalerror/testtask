<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('status')->delete();

        DB::table('status')->insert(['name' => 'Новый', 'slug' => 'new']);
        DB::table('status')->insert(['name' => 'В работе', 'slug' => 'onoperator']);
        DB::table('status')->insert(['name' => 'Подтвержден', 'slug' => 'accepted']);
    }
}
