<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('orders')->delete();

        DB::table('orders')->insert([
            'status_id'    => 1,
            'good_id'      => 1,
            'client_phone' => '777713522547',
            'client_name'  => 'John',
            'created_at'   => DB::Raw('now()'),
            'updated_at'   => DB::Raw('now()')
        ]);

        DB::table('orders')->insert([
            'status_id'    => 2,
            'good_id'      => 2,
            'client_phone' => '77756547656',
            'client_name'  => 'Michel',
            'created_at'   => DB::Raw('now()'),
            'updated_at'   => DB::Raw('now()')
        ]);

        DB::table('orders')->insert([
            'status_id'    => 3,
            'good_id'      => 3,
            'client_phone' => '77786789878',
            'client_name'  => 'Darrel',
            'created_at'   => DB::Raw('now()'),
            'updated_at'   => DB::Raw('now()')
        ]);

        DB::table('orders')->insert([
            'status_id'    => 1,
            'good_id'      => 4,
            'client_phone' => '77752456523',
            'client_name'  => 'Dan',
            'created_at'   => DB::Raw('now()'),
            'updated_at'   => DB::Raw('now()')
        ]);

    }
}
