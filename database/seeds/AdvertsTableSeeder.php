<?php

use Illuminate\Database\Seeder;

class AdvertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('adverts')->delete();

        DB::table('adverts')->insert([
            'first_name' => 'Иван',
            'last_name'  => 'Иванов',
            'login'      => 'advert1@yandex.ru',
            'password'   => '123123',
            'created_at' => DB::Raw('now()'),
            'updated_at' => DB::Raw('now()')
        ]);

        DB::table('adverts')->insert([
            'first_name' => 'Василий',
            'last_name'  => ' Васильев',
            'login'      => 'advert2@yandex.ru',
            'password'   => '123123',
            'created_at' => DB::Raw('now()'),
            'updated_at' => DB::Raw('now()')
        ]);

        DB::table('adverts')->insert([
            'first_name' => 'Петр',
            'last_name'  => 'Петров',
            'login'      => 'advert3@yandex.ru',
            'password'   => '123123',
            'created_at' => DB::Raw('now()'),
            'updated_at' => DB::Raw('now()')
        ]);

    }
}
