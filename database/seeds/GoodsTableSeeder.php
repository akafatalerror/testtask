<?php

use Illuminate\Database\Seeder;

class GoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('goods')->delete();

        DB::table('goods')->insert([
            'name'       => 'Часы Rado Integral',
            'price'      => '2000',
            'advert_id'  => '1',
            'created_at' => DB::Raw('now()'),
            'updated_at' => DB::Raw('now()')
        ]);
        DB::table('goods')->insert([
            'name'       => 'Часы Swiss Army',
            'price'      => '1500',
            'advert_id'  => '1',
            'created_at' => DB::Raw('now()'),
            'updated_at' => DB::Raw('now()')
        ]);
        DB::table('goods')->insert([
            'name'      => 'Детский планшет',
            'price'     => '2100',
            'advert_id' => '2',
            'created_at' => DB::Raw('now()'),
            'updated_at' => DB::Raw('now()')
        ]);

        DB::table('goods')->insert([
            'name'      => 'Колонки Monster Beats',
            'price'     => '900',
            'advert_id' => '3',
            'created_at' => DB::Raw('now()'),
            'updated_at' => DB::Raw('now()')

        ]);
    }
}
