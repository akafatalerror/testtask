<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->delete();
        Sentinel::register(array(
            'email'    => 'admin',
            'password' => '123123',
        ));

    }
}
