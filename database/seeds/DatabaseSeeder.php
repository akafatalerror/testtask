<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('StatusTableSeeder');
        $this->call('AdvertsTableSeeder');
        $this->call('GoodsTableSeeder');
        $this->call('OrdersTableSeeder');
        $this->call('UsersTableSeeder');
    }
}
