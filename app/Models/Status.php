<?php
namespace App\Models;

class Status extends \Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'status';


    /**
     * Возвращает массив статусов
     * @return array
     */
    public static function list(){

        $list = [];
        foreach (self::all() as $item) {
            $list[$item->slug] = $item->name;
        }

        return $list;
    }

    public function orders() {
        return $this->hasMany('App\Models\Order');
    }

}
