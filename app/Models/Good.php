<?php
namespace App\Models;

class Good extends \Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'goods';

    protected $fillable = array('name', 'price', 'advert_id');

    public static function rules(){
        return [
            'name'      => 'required',
            'price'     => 'required|numeric',
            'advert_id' => 'required|exists:adverts,id',
        ];
    }

    public function advert(){
        return $this->belongsTo('App\Models\Advert');
    }

    public function orders() {
        return $this->hasMany('App\Models\Order');
    }
}
