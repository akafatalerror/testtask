<?php
namespace App\Models;

class Order extends \Eloquent
{
    public function status(){
        return $this->belongsTo('App\Models\Status');
    }

    public function good(){
        return $this->belongsTo('App\Models\Good', 'good_id');
    }

}
