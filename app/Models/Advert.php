<?php
namespace App\Models;

class Advert extends \Eloquent
{

    public function orders() {
        return $this->hasMany('App\Models\Order');
    }

    public static function list() {
        $adverts_list = [];
        foreach (Advert::all() as  $item) {
            $adverts_list[$item->id] = $item->first_name.' '.$item->last_name.' / '.$item->login;
        }

        return $adverts_list;
    }

}
