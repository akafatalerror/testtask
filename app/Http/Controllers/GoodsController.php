<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Good;
use App\Models\Advert;
class GoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $list = Good::orderBy('name', 'asc')->get();



        return view('goods.list', ['list' => $list, ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('goods.add',['adverts_list' => Advert::list(), 'mode' => 'add']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, Good::rules());

         $good = Good::create([
             'name'      => $request->input('name'),
             'price'     => $request->input('price'),
             'advert_id' => $request->input('advert_id')
         ]);

         return redirect(route('goods.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $good = Good::find($id);

        if( !$good ){
            return redirect(route('goods.index'));
        }
        return view('goods.edit',['adverts_list' => Advert::list(), 'mode' => 'edit', 'good'=> $good]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //
         $this->validate($request, Good::rules());
         $good = Good::find($id);

         if( $good ){
            $good->fill([
                 'name'      => $request->input('name'),
                 'price'     => $request->input('price'),
                 'advert_id' => $request->input('advert_id')
            ])->save();
         }

         return redirect(route('goods.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
