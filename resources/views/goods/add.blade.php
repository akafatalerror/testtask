@extends('layouts.main')
@section('content')


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<h4>Добавить товар</h4>


<div>

    <?= Former::open(route('goods.store'))->method('POST') ?>
    {!! csrf_field() !!}
    <?= Former::text('name')->label('Название') ?><br>

    <?= Former::text('price')->label('Цена') ?><br>

    <?= Former::select('advert_id')->label('Рекламодатель')->options($adverts_list, 1)?>

    <div class="clearfix">
    <button type="submit" class="pull-right">Сохранить</button>
    </div>
</div>
<?= Former::close() ?>
@endsection
