@extends('layouts.main')
@section('content')


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<h4>Редактировать товар</h4>
<div>

    <?= Former::open(route('goods.update', ['id'=>$good->id]))->method('PUT') ?>
    {!! csrf_field() !!}
    <?= Former::text('name')->label('Название')->value($good->name) ?><br>

    <?= Former::text('price')->label('Цена')->value($good->price)?><br>

    <?= Former::select('advert_id')->label('Рекламодатель')->options($adverts_list, $good->advert_id)?>

    <div class="clearfix">
    <button type="submit" class="pull-right">Сохранить</button>
    </div>
</div>
<?= Former::close() ?>
@endsection
