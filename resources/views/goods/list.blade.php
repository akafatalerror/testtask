@extends('layouts.main')
@section('content')
    <div class="clearfix" style="margin-bottom:20px;">
        <a href="<?=route('goods.create')?>" class="btn btn-success pull-right">Добавить товар</a>
    </div>
    <?php if($list->count()):?>
        <table class="table">
            <thead>
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Цена</th>
                        <th>Рекламодатель</th>
                        <th></th>
                    </tr>

            </thead>
            <tbody>
                <?php foreach ($list as $item):?>
                    <tr>
                        <td><?=$item->id?></td>
                        <td><?=$item->name?></td>
                        <td><?=$item->price?></td>
                        <td>
                            <a href="#"><?=$item->advert->last_name?> <?=$item->advert->first_name?></a><br>
                            <?=$item->advert->login?>
                        </td>
                        <td>
                            <a href="<?=route('goods.edit',['id'=>$item->id])?>">Редактировать</a>
                        </td>

                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>

<script>
    $(document).ready(function(){
        $('.table').DataTable({
             "pageLength": 2,
             "language": {
                 "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Russian.json",

             }
        });
    });
</script>
    <?php else :?>
        <div class="empty_message">Заказов пока не создано</div>
    <?php endif;?>



@endsection
