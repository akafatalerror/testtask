@extends('layouts.main')
@section('content')

    <?php if($list->count()):?>
        <table class="table">
            <thead>
                    <tr>
                        <th>#</th>
                        <th>Дата</th>
                        <th>Клиент</th>
                        <th>Телефон</th>
                        <th>Товар</th>
                        <th>Статус</th>
                    </tr>

            </thead>
            <tbody>
                <?php foreach ($list as $item):?>
                    <tr>
                        <td><?=$item->id?></td>
                        <td><?=$item->created_at?></td>
                        <td><?=$item->client_name?></td>
                        <td><?=$item->client_phone?></td>
                        <td>
                            <a href="#"><?=$item->good->name?></a><br>
                            <?=$item->good->advert->last_name?> <?=$item->good->advert->first_name?> (<?=$item->good->advert->login?>)
                        </td>
                        <td><?=$item->status()->first()->name?></td>

                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>

        <script>
            $(document).ready(function(){
                $('.table').DataTable({
                     "pageLength": 2,
                     "language": {
                         "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Russian.json",

                     }
                });
            });
        </script>
    <?php else :?>
        <div class="empty_message">Заказов пока не создано</div>
    <?php endif;?>

@endsection
