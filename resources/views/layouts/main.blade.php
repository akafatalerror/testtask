
<!doctype html>
<html>
    <head>
        @section('meta')
            @include('common.meta')
        @show
    </head>
    <body>
        <div class="content">
            @include('common.menu')

            @yield('content')
        </div>
        @section('scripts')
            @include('common.scripts')
        @show
    </body>
</html>
